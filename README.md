# Documentation

The aim of this proyect is to demonstrate how to fully automate the deployment of a static site on AWS, using your own domain doint it automatically:  
- Using Infrastructure as Code.
- Using CI/CD pipelineson Gitlab to automate all changes.  
- Avoiding the use of AWS secrets using a federated access with an OIDC provider.

This would be the general architecture:  

![Architecture](architecture.png "Architecture")
